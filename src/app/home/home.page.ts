import {
	Component
} from '@angular/core';
import {
	SQLite,
	SQLiteObject
} from '@ionic-native/sqlite/ngx';

@Component({
	selector: 'app-home',
	templateUrl: 'home.page.html',
	styleUrls: ['home.page.scss'],
})
export class HomePage {

	selectData: string;
	constructor(private sqlite: SQLite) {
		//  this.sqliteCreate();
	}

	sqliteCreate() {
		this.sqlite.create({
				name: 'data.db',
				location: 'default'
			})
			.then((db: SQLiteObject) => {
				db.executeSql('CREATE TABLE IF NOT EXISTS lox_check (id INTEGER PRIMARY KEY AUTOINCREMENT,material_code varchar(50),agency_code varchar(50),nfc_tag_id varchar(100),qrc_identify varchar(100),status boolean,status_text varchar(100))', [])
					.then(() => console.log('Executed SQL'))
					.catch(e => console.log(e));
			})
			.catch(e => console.log(e));
	}

	sqlInsertData() {
		this.sqlite.create({
				name: 'data.db',
				location: 'default'
			})
			.then((db: SQLiteObject) => {

				db.transaction(function (tx) {
					tx.executeSql('INSERT INTO lox_check VALUES(material_code,agency_code,nfc_tag_id,qrc_identify,status,status_text)', ['1', '1', 'nfc1', 'qrc1', true, 'MATERIAL FOUND']);
				}).then(() => {
						console.log("execute");
					},
					(error) => {
						console.log(" error execute");
					}
				).catch((error) => {
					console.log("catch error execute");
					console.log(error);

				});

			})
			.catch(e => console.log(e));
	}
	sqlSelectData() {
		this.sqlite.create({
				name: 'data.db',
				location: 'default'
			})
			.then((db: SQLiteObject) => {

				db.transaction(function (tx) {
          tx.executeSql('SELECT *  FROM lox_check', [], function(tx, rs) {

            console.log('Record count (expected to be 2): ' + rs.rows.item(0));

          }, function(tx, error) {
            console.log('SELECT error: ' + error.message);
          });
          
				}).then(() => {
						console.log("execute");
					},
					(error) => {
						console.log(" error execute");
					}
				).catch((error) => {
					console.log("catch error execute");
					console.log(error);

				});

			})
			.catch(e => console.log(e));
	}
}